Put the directory "game" to the directory "${STEAM_APPS}/Coming Out on Top", combine with the existing "game" and overwrite any existing file.

If there are *.rpy and *.rpyc files under "${STEAM_APPS}/Coming Out on Top/game", remove them.

If the game cannot start, remove all the saved files in both below 2 directories.
1. C:/Users/${USER_ID}/AppData/Roaming/RenPy/Coming-Out-On-Top
2. ${STEAM_APPS}/Coming Out on Top/game/saves
